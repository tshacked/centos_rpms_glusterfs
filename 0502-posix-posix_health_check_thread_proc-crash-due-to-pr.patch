From 11f9ffcd733c95e8a728c150ff2ffc3dbeaddac1 Mon Sep 17 00:00:00 2001
From: Mohit Agrawal <moagrawa@redhat.com>
Date: Fri, 11 Jan 2019 11:57:20 +0530
Subject: [PATCH 502/506] posix: posix_health_check_thread_proc crash due to
 priv is NULL

Problem: posix_fini sends a cancellation request to health_check
         thread and cleanup priv without ensuring health_check thread
         is running

Solution: Make health_check && disk_space thread joinable and call
          gf_thread_cleanup_xint to wait unless thread is not finished

> Change-Id: I4d37b08138766881dab0922a47ed68a2c3411f13
> fixes: bz#1636570
> (Reviewed on upstream link https://review.gluster.org/#/c/glusterfs/+/21717/)
> (Cherry pick from commit e82bcc33ed2d5cd54d3f918397f31818089299ad)

Change-Id: I9edadd5bc445549b5f45bab98e4794d62a185f1c
BUG: 1662828
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/160404
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 xlators/storage/posix/src/posix-helpers.c |  8 +++-----
 xlators/storage/posix/src/posix.c         | 15 +++++++++++----
 2 files changed, 14 insertions(+), 9 deletions(-)

diff --git a/xlators/storage/posix/src/posix-helpers.c b/xlators/storage/posix/src/posix-helpers.c
index cddf02e63..ed5d3e55e 100644
--- a/xlators/storage/posix/src/posix-helpers.c
+++ b/xlators/storage/posix/src/posix-helpers.c
@@ -1854,8 +1854,6 @@ posix_spawn_health_check_thread (xlator_t *xl)
                         goto unlock;
                 }
 
-                /* run the thread detached, resources will be freed on exit */
-                pthread_detach (priv->health_check);
                 priv->health_check_active = _gf_true;
         }
 unlock:
@@ -1958,9 +1956,9 @@ posix_spawn_disk_space_check_thread (xlator_t *xl)
                         priv->disk_space_check_active = _gf_false;
                 }
 
-                ret = gf_thread_create_detached (&priv->disk_space_check,
-                                                 posix_disk_space_check_thread_proc,
-                                                 xl, "posix_reserve");
+                ret = gf_thread_create (&priv->disk_space_check, NULL,
+                                        posix_disk_space_check_thread_proc,
+                                        xl, "posix_reserve");
                 if (ret < 0) {
                         priv->disk_space_check_active = _gf_false;
                         gf_msg (xl->name, GF_LOG_ERROR, errno,
diff --git a/xlators/storage/posix/src/posix.c b/xlators/storage/posix/src/posix.c
index 13b4aa6b6..591119ea9 100644
--- a/xlators/storage/posix/src/posix.c
+++ b/xlators/storage/posix/src/posix.c
@@ -7999,18 +7999,25 @@ void
 fini (xlator_t *this)
 {
         struct posix_private *priv = this->private;
+        gf_boolean_t health_check = _gf_false;
+
         if (!priv)
                 return;
         LOCK (&priv->lock);
-        if (priv->health_check_active) {
+        {
+                health_check = priv->health_check_active;
                 priv->health_check_active = _gf_false;
-                pthread_cancel (priv->health_check);
-                priv->health_check = 0;
         }
         UNLOCK (&priv->lock);
+
+        if (health_check) {
+                (void)gf_thread_cleanup_xint(priv->health_check);
+                priv->health_check = 0;
+        }
+
         if (priv->disk_space_check) {
                 priv->disk_space_check_active = _gf_false;
-                pthread_cancel (priv->disk_space_check);
+                (void)gf_thread_cleanup_xint(priv->disk_space_check);
                 priv->disk_space_check = 0;
         }
         if (priv->janitor) {
-- 
2.20.1

