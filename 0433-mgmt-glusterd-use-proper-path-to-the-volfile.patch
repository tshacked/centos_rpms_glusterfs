From fd9e0103cd5c3f2962e063dbc3083c451b7e592b Mon Sep 17 00:00:00 2001
From: Raghavendra Bhat <raghavendra@redhat.com>
Date: Thu, 4 Oct 2018 14:27:45 -0400
Subject: [PATCH 433/444] mgmt/glusterd: use proper path to the volfile

    > Upstream: https://review.gluster.org/#/c/glusterfs/+/21314/
    > BUG: 1635050
    > Change-Id: I28b2dfa5d9b379fe943db92c2fdfea879a6a594e

NOTE: This patch is actually directly applied from the patch that
      was sent to the release-4.1 branch. The master branch patch
      will have merge conflicts due to the clang format changes done
      there. This is the patch which this commit is a backport of.

      upstream(4.1): https://review.gluster.org/#/c/glusterfs/+/21348/
      Chane-ID: I28b2dfa5d9b379fe943db92c2fdfea879a6a594e

Till now, glusterd was generating the volfile path for the snapshot
volume's bricks like this.

/snaps/<snap name>/<brick volfile>

But in reality, the path to the brick volfile for a snapshot volume is

/snaps/<snap name>/<snap volume name>/<brick volfile>

The above workaround was used to distinguish between a mount command used
to mount the snapshot volume, and a brick of the snapshot volume, so that
based on what is actually happening, glusterd can return the proper volfile
(client volfile for the former and the brick volfile for the latter). But,
this was causing problems for snapshot restore when brick multiplexing is
enabled. Because, with brick multiplexing, it tries to find the volfile
and sends GETSPEC rpc call to glusterd using the 2nd style of path i.e.

/snaps/<snap name>/<snap volume name>/<brick volfile>

So, when the snapshot brick (which is multiplexed) sends a GETSPEC rpc
request to glusterd for obtaining the brick volume file, glusterd was
returning the client volume file of the snapshot volume instead of the
brick volume file.

Change-Id: I28b2dfa5d9b379fe943db92c2fdfea879a6a594e
BUG: 1636291
Signed-off-by: Raghavendra Bhat <raghavendra@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/155129
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 .../snapview-server/src/snapview-server-helpers.c    |  5 +++--
 xlators/mgmt/glusterd/src/glusterd-handshake.c       | 20 ++++++++++++++++++--
 xlators/mgmt/glusterd/src/glusterd-utils.c           |  9 +++++----
 3 files changed, 26 insertions(+), 8 deletions(-)

diff --git a/xlators/features/snapview-server/src/snapview-server-helpers.c b/xlators/features/snapview-server/src/snapview-server-helpers.c
index 2ad74ef..4c2edc6 100644
--- a/xlators/features/snapview-server/src/snapview-server-helpers.c
+++ b/xlators/features/snapview-server/src/snapview-server-helpers.c
@@ -481,8 +481,9 @@ __svs_initialise_snapshot_volume (xlator_t *this, const char *name,
                 goto out;
         }
 
-        snprintf (volname, sizeof (volname), "/snaps/%s/%s",
-                  dirent->name, dirent->snap_volname);
+        snprintf (volname, sizeof (volname), "/snaps/%s/%s/%s",
+                  dirent->name, dirent->snap_volname,
+                  dirent->snap_volname);
 
 
         fs = glfs_new (volname);
diff --git a/xlators/mgmt/glusterd/src/glusterd-handshake.c b/xlators/mgmt/glusterd/src/glusterd-handshake.c
index d5594d0..b2a9b20 100644
--- a/xlators/mgmt/glusterd/src/glusterd-handshake.c
+++ b/xlators/mgmt/glusterd/src/glusterd-handshake.c
@@ -52,6 +52,7 @@ get_snap_volname_and_volinfo (const char *volpath, char **volname,
         char            *vol            = NULL;
         glusterd_snap_t *snap           = NULL;
         xlator_t        *this           = NULL;
+        char            *volfile_token  = NULL;
 
         this = THIS;
         GF_ASSERT (this);
@@ -101,12 +102,27 @@ get_snap_volname_and_volinfo (const char *volpath, char **volname,
          */
         ret = glusterd_volinfo_find (volname_token, volinfo);
         if (ret) {
-                *volname = gf_strdup (volname_token);
+                gf_msg (this->name, GF_LOG_WARNING, 0, GD_MSG_VOLINFO_GET_FAIL,
+                        "failed to get the volinfo for the volume %s",
+                        volname_token);
+
+                /* Get the actual volfile name */
+                volfile_token = strtok_r (NULL, "/", &save_ptr);
+                *volname = gf_strdup (volfile_token);
                 if (NULL == *volname) {
                         ret = -1;
                         goto out;
                 }
 
+                        /*
+                         * Ideally, this should succeed as volname_token now
+                         * contains the name of the snap volume (i.e. name of
+                         * the volume that represents the snapshot).
+                         * But, if for some reason, volinfo for the snap volume
+                         * is not found, then try to get from the name of the
+                         * volfile. Name of the volfile is like this.
+                         * <snap volume name>.<hostname>.<brick path>.vol
+                         */
                 ret = glusterd_snap_volinfo_find (volname_token, snap,
                                                   volinfo);
                 if (ret) {
@@ -115,7 +131,7 @@ get_snap_volname_and_volinfo (const char *volpath, char **volname,
                         if (!vol) {
                                 gf_msg (this->name, GF_LOG_ERROR, EINVAL,
                                         GD_MSG_INVALID_ENTRY, "Invalid "
-                                                "volname (%s)", volname_token);
+                                                "volname (%s)", volfile_token);
                                 goto out;
                         }
 
diff --git a/xlators/mgmt/glusterd/src/glusterd-utils.c b/xlators/mgmt/glusterd/src/glusterd-utils.c
index 04fae63..7179a68 100644
--- a/xlators/mgmt/glusterd/src/glusterd-utils.c
+++ b/xlators/mgmt/glusterd/src/glusterd-utils.c
@@ -2068,10 +2068,10 @@ retry:
         }
 
         if (volinfo->is_snap_volume) {
-                snprintf (volfile, PATH_MAX,"/%s/%s/%s.%s.%s",
+                snprintf (volfile, PATH_MAX, "/%s/%s/%s/%s.%s.%s",
                           GLUSTERD_VOL_SNAP_DIR_PREFIX,
                           volinfo->snapshot->snapname, volinfo->volname,
-                          brickinfo->hostname, exp_path);
+                          volinfo->volname, brickinfo->hostname, exp_path);
         } else {
                 snprintf (volfile, PATH_MAX, "%s.%s.%s", volinfo->volname,
                           brickinfo->hostname, exp_path);
@@ -5676,10 +5676,11 @@ attach_brick (xlator_t *this,
         GLUSTERD_GET_BRICK_PIDFILE (pidfile2, volinfo, brickinfo, conf);
 
         if (volinfo->is_snap_volume) {
-                snprintf (full_id, sizeof(full_id), "/%s/%s/%s.%s.%s",
+                snprintf (full_id, sizeof(full_id), "/%s/%s/%s/%s.%s.%s",
                           GLUSTERD_VOL_SNAP_DIR_PREFIX,
                           volinfo->snapshot->snapname,
-                          volinfo->volname, brickinfo->hostname, unslashed);
+                          volinfo->volname, volinfo->volname,
+                          brickinfo->hostname, unslashed);
         } else {
                 snprintf (full_id, sizeof(full_id), "%s.%s.%s",
                           volinfo->volname, brickinfo->hostname, unslashed);
-- 
1.8.3.1

