From 61d178c25468adfac4cbdfcef43a0d002c635466 Mon Sep 17 00:00:00 2001
From: Mohit Agrawal <moagrawa@redhat.com>
Date: Tue, 5 Feb 2019 12:49:10 +0530
Subject: [PATCH 513/529] core: heketi-cli is throwing error "target is busy"

Problem: When rpc-transport-disconnect happens, server_connection_cleanup_flush_cbk()
         is supposed to call rpc_transport_unref() after open-files on
         that transport are flushed per transport.But open-fd-count is
         maintained in bound_xl->fd_count, which can be incremented/decremented
         cumulatively in server_connection_cleanup() by all transport
         disconnect paths. So instead of rpc_transport_unref() happening
         per transport, it ends up doing it only once after all the files
         on all the transports for the brick are flushed leading to
         rpc-leaks.

Solution: To avoid races maintain fd_cnt at client instead of maintaining
          on brick

Credits: Pranith Kumar Karampuri
> Change-Id: I6e8ea37a61f82d9aefb227c5b3ab57a7a36850e6
> fixes: bz#1668190
> (Cherry pick from commit b41cdeb638f9f9ec2fef13ec95c216faf52a9df9)
> (Reviewed on upstream link https://review.gluster.org/#/c/glusterfs/+/22108/)

Change-Id: Ic810095ea1ce418836d240d411168df8be0e4a41
BUG: 1669020
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/162251
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 libglusterfs/src/client_t.c                  | 1 +
 libglusterfs/src/client_t.h                  | 2 ++
 libglusterfs/src/xlator.c                    | 1 -
 libglusterfs/src/xlator.h                    | 3 ---
 xlators/protocol/server/src/server-helpers.c | 4 ++--
 5 files changed, 5 insertions(+), 6 deletions(-)

diff --git a/libglusterfs/src/client_t.c b/libglusterfs/src/client_t.c
index a9ae62c..7d92e0d 100644
--- a/libglusterfs/src/client_t.c
+++ b/libglusterfs/src/client_t.c
@@ -232,6 +232,7 @@ gf_client_get (xlator_t *this, struct rpcsvc_auth_data *cred, char *client_uid,
 
                 GF_ATOMIC_INIT (client->bind, 1);
                 GF_ATOMIC_INIT (client->count, 1);
+                GF_ATOMIC_INIT(client->fd_cnt, 0);
 
                 client->auth.flavour = cred->flavour;
                 if (cred->flavour != AUTH_NONE) {
diff --git a/libglusterfs/src/client_t.h b/libglusterfs/src/client_t.h
index 088508e..403c488 100644
--- a/libglusterfs/src/client_t.h
+++ b/libglusterfs/src/client_t.h
@@ -46,6 +46,8 @@ typedef struct _client {
         inode_t *subdir_inode;
         uuid_t   subdir_gfid;
         int32_t  opversion;
+        /* Variable to save fd_count for detach brick */
+        gf_atomic_t fd_cnt;
 } client_t;
 
 #define GF_CLIENTCTX_INITIAL_SIZE 8
diff --git a/libglusterfs/src/xlator.c b/libglusterfs/src/xlator.c
index 340d83d..7d90baa 100644
--- a/libglusterfs/src/xlator.c
+++ b/libglusterfs/src/xlator.c
@@ -490,7 +490,6 @@ xlator_init (xlator_t *xl)
 
         xl->instance_name = NULL;
         GF_ATOMIC_INIT(xl->xprtrefcnt, 0);
-        GF_ATOMIC_INIT(xl->fd_cnt, 0);
         if (!xl->init) {
                 gf_msg (xl->name, GF_LOG_WARNING, 0, LG_MSG_INIT_FAILED,
                         "No init() found");
diff --git a/libglusterfs/src/xlator.h b/libglusterfs/src/xlator.h
index f8f2630..202bdca 100644
--- a/libglusterfs/src/xlator.h
+++ b/libglusterfs/src/xlator.h
@@ -965,9 +965,6 @@ struct _xlator {
         /* flag to avoid recall of xlator_mem_cleanup for xame xlator */
         uint32_t call_cleanup;
 
-        /* Variable to save fd_count for detach brick */
-        gf_atomic_t fd_cnt;
-
         /* Variable to save xprt associated for detach brick */
         gf_atomic_t xprtrefcnt;
 
diff --git a/xlators/protocol/server/src/server-helpers.c b/xlators/protocol/server/src/server-helpers.c
index 99256bf..30045ef 100644
--- a/xlators/protocol/server/src/server-helpers.c
+++ b/xlators/protocol/server/src/server-helpers.c
@@ -266,7 +266,7 @@ server_connection_cleanup_flush_cbk (call_frame_t *frame, void *cookie,
                 victim = client->bound_xl;
 
         if (victim) {
-                fd_cnt = GF_ATOMIC_DEC(victim->fd_cnt);
+                fd_cnt = GF_ATOMIC_DEC(client->fd_cnt);
                 if (!fd_cnt && conf && detach) {
                         pthread_mutex_lock(&conf->mutex);
                         {
@@ -413,7 +413,7 @@ server_connection_cleanup (xlator_t *this, client_t *client,
                         if (fd_cnt) {
                                 if (fd_exist)
                                         (*fd_exist) = _gf_true;
-                                GF_ATOMIC_ADD(bound_xl->fd_cnt, fd_cnt);
+                                GF_ATOMIC_ADD(client->fd_cnt, fd_cnt);
                         }
                 }
 
-- 
1.8.3.1

