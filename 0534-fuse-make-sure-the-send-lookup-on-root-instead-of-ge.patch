From 1454b4a477bf841d6bea610b3bb0b084730d38ab Mon Sep 17 00:00:00 2001
From: Amar Tumballi <amarts@redhat.com>
Date: Fri, 13 Apr 2018 10:28:01 +0530
Subject: [PATCH 534/534] fuse: make sure the send lookup on root instead of
 getattr()

This change was done in https://review.gluster.org/16945. While the
changes added there were required, it was not necessary to remove the
getattr part. As fuse's lookup on root(/) comes as getattr only, this
change is very much required.

The previous fix for this bug was to add the check for revalidation in
lookup when it was sent on root. But I had removed the part where
getattr is coming on root. The removing was not requried to fix the
issue then. Added back this part of the code, to make sure we have
proper validation of root inode in many places like acl, etc.

upstream patch: https://review.gluster.org/#/c/glusterfs/+/19867/

> updates: bz#1437780
> Change-Id: I859c4ee1a3f407465cbf19f8934530848424ff50
> Signed-off-by: Amar Tumballi <amarts@redhat.com>

Change-Id: I85af744e7bd1a52367d85d5a5b07c4bdf409a66d
BUG: 1668327
Signed-off-by: N Balachandran <nbalacha@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/164663
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 xlators/mount/fuse/src/fuse-bridge.c | 20 ++++++++++++++++++++
 1 file changed, 20 insertions(+)

diff --git a/xlators/mount/fuse/src/fuse-bridge.c b/xlators/mount/fuse/src/fuse-bridge.c
index 175e1b8..ffc1013 100644
--- a/xlators/mount/fuse/src/fuse-bridge.c
+++ b/xlators/mount/fuse/src/fuse-bridge.c
@@ -1311,6 +1311,7 @@ fuse_getattr (xlator_t *this, fuse_in_header_t *finh, void *msg,
         fuse_private_t         *priv = NULL;
 #endif
         fuse_state_t *state;
+        int ret = -1;
 
         GET_STATE (this, finh, state);
 #if FUSE_KERNEL_MINOR_VERSION >= 9
@@ -1318,6 +1319,25 @@ fuse_getattr (xlator_t *this, fuse_in_header_t *finh, void *msg,
         if (priv->proto_minor >= 9 && fgi->getattr_flags & FUSE_GETATTR_FH)
                 state->fd = fd_ref ((fd_t *)fgi->fh);
 #endif
+        if (finh->nodeid == 1) {
+                state->gfid[15] = 1;
+
+                ret = fuse_loc_fill (&state->loc, state, finh->nodeid, 0, NULL);
+                if (ret < 0) {
+                        gf_log ("glusterfs-fuse", GF_LOG_WARNING,
+                                "%"PRIu64": GETATTR on / (fuse_loc_fill() failed)",
+                                finh->unique);
+                        send_fuse_err (this, finh, ENOENT);
+                        free_fuse_state (state);
+                        return;
+                }
+
+                fuse_gfid_set (state);
+
+                FUSE_FOP (state, fuse_root_lookup_cbk, GF_FOP_LOOKUP,
+                          lookup, &state->loc, state->xdata);
+                return;
+        }
 
         if (state->fd)
                 fuse_resolve_fd_init (state, &state->resolve, state->fd);
-- 
1.8.3.1

