From ce2c9ea016ffa20bf291264a012cc14102040900 Mon Sep 17 00:00:00 2001
From: Milind Changire <mchangir@redhat.com>
Date: Mon, 10 Sep 2018 13:48:18 +0530
Subject: [PATCH 421/444] rpc: handle EAGAIN when SSL_ERROR_SYSCALL is returned

Problem:
A return value of ENODATA was forcibly returned in the case where
SSL_get_error(r) returned SSL_ERROR_SYSCALL. Sometimes SSL_ERROR_SYSCALL
is a transient error which is identified by setting errno to EAGAIN.
EAGAIN is not a fatal error and indicates that the syscall needs to be
retried.

Solution:
Bubble up the errno in case SSL_get_error(r) returns SSL_ERROR_SYSCALL
and let the upper layers handle it appropriately.

mainline:
> Reviewed-on: https://review.gluster.org/c/glusterfs/+/20993
> fixes: bz#1622405
> Change-Id: I76eff278378930ee79abbf9fa267a7e77356eed6
> BUG: 1622405

Change-Id: I76eff278378930ee79abbf9fa267a7e77356eed6
BUG: 1622308
Signed-off-by: Milind Changire <mchangir@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/154868
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 rpc/rpc-transport/socket/src/socket.c | 11 +++++++++--
 1 file changed, 9 insertions(+), 2 deletions(-)

diff --git a/rpc/rpc-transport/socket/src/socket.c b/rpc/rpc-transport/socket/src/socket.c
index 8a08177..34a937f 100644
--- a/rpc/rpc-transport/socket/src/socket.c
+++ b/rpc/rpc-transport/socket/src/socket.c
@@ -209,6 +209,7 @@ ssl_do (rpc_transport_t *this, void *buf, size_t len, SSL_trinary_func *func)
 	int               r = (-1);
 	struct pollfd     pfd = {-1,};
 	socket_private_t *priv = NULL;
+        int               myerrno = -1;
 
 	GF_VALIDATE_OR_GOTO(this->name,this->private,out);
 	priv = this->private;
@@ -276,10 +277,16 @@ ssl_do (rpc_transport_t *this, void *buf, size_t len, SSL_trinary_func *func)
 			}
 			break;
 		case SSL_ERROR_SYSCALL:
+                        myerrno = errno;
 			/* This is what we get when remote disconnects. */
 			gf_log(this->name,GF_LOG_DEBUG,
-			       "syscall error (probably remote disconnect)");
-			errno = ENODATA;
+			       "syscall error (probably remote disconnect)"
+                               " errno:%d(%s)", errno, strerror(errno));
+                        /* sometimes, errno is set to EAGAIN in this case
+                         * so let the upper layers do what they need to do
+                         * with it
+                         */
+			errno = myerrno;
 			goto out;
 		default:
 			errno = EIO;
-- 
1.8.3.1

