From a064614b60924c4b0b1dbc4dd18278ce18b46db0 Mon Sep 17 00:00:00 2001
From: Xavi Hernandez <xhernandez@redhat.com>
Date: Fri, 6 Jul 2018 23:26:41 +0200
Subject: [PATCH 462/493] posix: Do not log ENXIO errors for seek fop

When lseek is used with SEEK_DATA and SEEK_HOLE, it's expected that the
last operation fails with ENXIO when offset is beyond the end of file.
In this case it doesn't make sense to report this as an error log message.

This patch reports ENXIO failure messages for seek fops in debug level
instead of error level.

> Change-Id: I62a4f61f99b0e4d7ea6a2cdcd40afe15072794ac
> fixes: bz#1598926
> Signed-off-by: Xavi Hernandez <xhernandez@redhat.com>

Upstream patch: https://review.gluster.org/c/glusterfs/+/20475
Change-Id: I62a4f61f99b0e4d7ea6a2cdcd40afe15072794ac
BUG: 1598883
Signed-off-by: Xavi Hernandez <xhernandez@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/158531
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 libglusterfs/src/common-utils.c   | 6 ++++++
 xlators/storage/posix/src/posix.c | 3 ++-
 2 files changed, 8 insertions(+), 1 deletion(-)

diff --git a/libglusterfs/src/common-utils.c b/libglusterfs/src/common-utils.c
index dd6cdb3..25600a9 100644
--- a/libglusterfs/src/common-utils.c
+++ b/libglusterfs/src/common-utils.c
@@ -4369,6 +4369,12 @@ fop_log_level (glusterfs_fop_t fop, int op_errno)
                 if (op_errno == EEXIST)
                         return GF_LOG_DEBUG;
 
+        if (fop == GF_FOP_SEEK) {
+                if (op_errno == ENXIO) {
+                        return GF_LOG_DEBUG;
+                }
+        }
+
         return GF_LOG_ERROR;
 }
 
diff --git a/xlators/storage/posix/src/posix.c b/xlators/storage/posix/src/posix.c
index e46fe99..13b4aa6 100644
--- a/xlators/storage/posix/src/posix.c
+++ b/xlators/storage/posix/src/posix.c
@@ -1225,7 +1225,8 @@ posix_seek (call_frame_t *frame, xlator_t *this, fd_t *fd, off_t offset,
         ret = sys_lseek (pfd->fd, offset, whence);
         if (ret == -1) {
                 err = errno;
-                gf_msg (this->name, GF_LOG_ERROR, err, P_MSG_SEEK_FAILED,
+                gf_msg (this->name, fop_log_level(GF_FOP_SEEK, err), err,
+                        P_MSG_SEEK_FAILED,
                         "seek failed on fd %d length %" PRId64 , pfd->fd,
                         offset);
                 goto out;
-- 
1.8.3.1

